# README #

Este es un documento para el deber de Casting y Exceptions de OCJP


### Ejercicio ###

##Legal and Illegal Casts ##

Write an application that illustrates legal and Illegal casts. Work with the following class/ 
interface hierarchy: 
class Fruit 
class Apple extends Fruit 
interface Squeezable c
lass Citrus extends Fruit implements Squeezable 
class Orange extends Citrus 

You will have to define the classes and the interface, but the definitions can be empty. 
Your application should construct one instance of each of the following classes: 
- Object
- Fruit
- Apple
- Citrus
- Orange 
Try to cast each of these objects to the following types: 
- Fruit
- Apple
- Squeezable
- Citrus
- Orange 
For each attempted cast, print out a message stating whether the cast succeeded. 
(A ClassCastException is thrown if the cast failed; if no exception is thrown, the cast succeeded.) 
A fragment of the output of the sample solution (Caster.java on the CD-ROM) looks like this: 

Checking casts for FruitFruit: OKApple: NOSqueezabl e: NOCitrus: NOOrange: NO 
Checking casts for AppleFruit: OKApple: OKSqueezabl e: NOCitrus: NOOrange: NO