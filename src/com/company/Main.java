package com.company;


public class Main {

    public static void main(String[] args) {

//Construcción de instancias de cada una de las clases:

        Object obj = new Object();
        Apple apple=new Apple();
        Citrus citrus =new Citrus();
        Fruit fruit=new Fruit();
        Orange orange= new Orange();
        Squeezable s ;

//cast => Citrus:Orange, Orange:Citrus

        try {
            Citrus c = new Citrus();
            c = orange;
            System.out.println("Citrus:Orange="+"OK");
        }catch (Exception ex){
            System.out.println("Citrus:Orange="+"NO");
        }
        try {
            Citrus c2 = new Citrus();
            orange= (Orange) c2;
            System.out.println("Orange:Citrus="+"OK");
        }catch (Exception ex){
            System.out.println("Orange:Citrus="+"NO");
        }

//cast => Citrus:Apple, Apple:Citrus

        try {
            Citrus c = new Citrus();
            System.out.println("Citrus:Apple="+"NO");
        }catch (Exception ex){
            System.out.println("Citrus:Apple="+"No");
        }
        try {
            Citrus c2 = new Citrus();
            System.out.println("Apple:Citrus="+"NO");
        }catch (Exception ex){
            System.out.println("Apple:Citrus="+"NO");
        }

//cast => apple:Squeezable

        try{
            Apple ap=new Apple();
            Squeezable sq =(Squeezable) ap;
            System.out.println("Apple:Squeezable=OK");
        }catch (ClassCastException e){
            System.out.println("Apple:Squeezable=NO");
        }

//cast => Orange:Fruit

        try{
            Orange or=new Orange();
            Fruit fr =(Fruit)or;
            System.out.println("Orange:Fruit=OK");
        }catch (ClassCastException e){
            System.out.println("Orange:Fruit=NO");
        }

//cast => Fruit: Squeezable

        try{
            Fruit fr = new Fruit();
            Squeezable sq=(Squeezable) fr;
            System.out.println("Fruit: Squeezable=OK");
        }catch (ClassCastException e){
            System.out.println("Fruit: Squeezable=NO");
        }

//cast => Apple:Fruit

        try{
            Apple App = new Apple();
            Fruit fr= (Fruit)App;
            System.out.println("Apple:Fruit=OK");
        }catch (ClassCastException e){
            System.out.println("Apple:Fruit=NO");
        }
    }
}
